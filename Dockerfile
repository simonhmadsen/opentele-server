# start from base
FROM tomcat:7-jre7
MAINTAINER Simon Madsen <madsen@alexandra.dk>

# install needed applications
RUN apt-get -yqq update
RUN apt-get -yqq install git
RUN apt-get -yqq install maven
RUN apt-get -yqq install default-jdk

RUN apt-get -qq update
RUN apt-get -qqy install iputils-ping

# set java home
ENV JAVA_HOME /usr/lib/jvm/default-java

# install curl and unzip curl and unzip
RUN apt-get update && apt-get install -yqq --no-install-recommends wget tar curl unzip

# install sdkman
RUN curl -s http://get.sdkman.io | bash

# install grails
ENV GRAILS_VERSION 2.4.4

RUN bash -lc "sdk install grails $GRAILS_VERSION"

# Clean up APT.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV CATALINA_OPTS="$CATALINA_OPTS -Xms1024m -Xmx2048m -XX:PermSize=256m -XX:MaxPermSize=356m"

# expose port
EXPOSE 8080

# get application code
RUN git clone https://bitbucket.org/4s/opentele2-server /opt/opentele2-server
WORKDIR /opt/opentele2-server

# create configuration files for OpenTele
RUN mkdir -p /root/.opentele
ADD clinician.properties /root/.opentele

RUN mkdir -p /var/lib/tomcat7/lib/
ADD log4j.properties /var/lib/tomcat7/lib/

# get dependencies
RUN git clone https://bitbucket.org/4s/kih_auditlog /opt/kih_auditlog && cd /opt/kih_auditlog && bash -lc "grails compile"
RUN cd /opt/kih_auditlog && bash -lc "grails maven-install"
RUN git clone https://bitbucket.org/4s/opentele2-server-core-plugin /opt/opentele2-server-core-plugin && cd /opt/opentele2-server-core-plugin && bash -lc "grails compile"
RUN cd /opt/opentele2-server-core-plugin && bash -lc "grails maven-install"

# start app
RUN bash -lc "grails dev war" &&  cp /opt/opentele2-server/target/opentele-server.war /usr/local/tomcat/webapps