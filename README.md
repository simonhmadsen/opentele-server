# README #
This repository contains all necessary Dockerfiles and docker-compose.yml's to run an opentele-server container with a MySQL database in another container

## How do I get set up? ##

### Prerequisites ###
* Docker
* Docker-compose

### Steps ###

* Clone the repository to a folder on your local machine
* CD into the folder
* Execute the command 
```
#!java

docker-compose up
```

After going through the above steps, the opentele server, is reachable at localhost:8080/opentele-server
The database is reachable at localhost:3306 with user root and password root